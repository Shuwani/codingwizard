import java.util.Scanner;

public class ques05{
		int x, y, width, height;
		public ques05(int x, int y, int width, int height) {
			this.x = x;
			this.y = y;
			this. width = width;
			this. height = height;
		}
		public static boolean isIntersect(ques05 R1 , ques05 R2) {
			return R1.x <= R2.x + R2.width && R1.x + R1.width >= R2.x
					&& R1.y <= R2.y + R2.height && R1.y + R1.height >= R2.y;
		}
		public static void display(ques05 a , ques05 b) {
			if (isIntersect(a,b)) {
				System.out.println("Rectangle is Intersecting");
			}
			else {
				System.out.println("Rectangle is not Intersecting");
			}
		}
		public static void main(String[] args) {
			ques05 r1=new ques05(1,1,1,1);
			ques05 r2=new ques05(2,2,1,1);
			ques05.display(r1,r2);
		}
}