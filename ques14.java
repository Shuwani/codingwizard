import java.util.Scanner;

public class ques14 {
	public static void loop(int n) {
		for(int i=1;i<=10;i++) {
			System.out.print(i+" ");
		}
	}
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
		if(n>10) {
			loop(n);
			int i=10,a=12,b;
			while(i<n) {
				b=a+9;
				System.out.print(a+" ");
				System.out.print(b+" ");
				a=b+2;
				i++;
			}
		}
		else
			loop(n);
	}
}