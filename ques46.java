import java.util.Scanner;

public class ques46 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter three numbers");
		double a=sc.nextDouble();
		double b=sc.nextDouble();
		double c=sc.nextDouble();
		roots(a,b,c);
	}
	public static void roots(double a,double b,double c) {
		double r1, r2;
		double det = b*b-(4*a*c);
		if (det > 0) {
			r1 = (-b + Math.sqrt(det))/(2 * a);
            r2 = (-b - Math.sqrt(det))/(2 * a);
            System.out.println("root1= "+r1+"  "+"root2= "+r2);
		}
		else if (det == 0) {
			r1 = r2 = -b/(2*a);
			System.out.format("root1=root2=%.2f",r1);
			}
		else {
			double r = -b / (2 * a);
            double imag = Math.sqrt(-det) / (2 * a);
            System.out.format("root1 = %.2f+%.2fi", r, imag);
            System.out.format("\nroot2 = %.2f-%.2fi", r, imag);
		}
	}
}
