public class ques82 {

	public static void main(String[] args) {
		int a[]= {0,0,0,0,1};
		int res=transition(a);
		if(res>0)
			System.out.println(res+" is the point from where 1 starts.");
		else
			System.out.println(res+" because there is no transition point.");
	}
	
	public static int transition(int a[]) {
		for(int i=0;i<a.length-1;i++) {
			if(a[i]!=a[i+1]) {
				return i+1;
			}
			}
		return -1;
	}
}