import java.util.Scanner;

public class ques61 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the numbers");
		double n=sc.nextDouble();
		double r=sc.nextDouble();
		System.out.println(root(n,r));
	}
	public static double root(double n, double r){
		double x = n;
		double sq;
		int count=0;
		while (true){
			count++;
			double s=x+(n/x);
			sq=0.5*s;
			if (Math.abs(sq-x)<r) {
				break;
			}
			x=sq;
			}
		return sq;
		}
	}