import java.util.Scanner;

public class ques44 {
	public static boolean Palindrome(long n){
		long num=n;
		long rev = 0;
		while (num > 0){
			rev <<= 1;
			if ((num & 1) == 1) {
				rev ^= 1;
			}
			num >>= 1;
			}
		if(n==rev)
			return true;
		else
			return false;
	}
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		long n=sc.nextLong();
		if (Palindrome(n))
			System.out.println("Yes");
		else
			System.out.println("No");
	}
}