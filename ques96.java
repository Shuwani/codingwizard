import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ques96 {
	public static void main(String[] args) {
		BigInteger sum=BigInteger.valueOf(0);
		for(int i=1;i<=1000;i++) {
			BigInteger b1=BigInteger.valueOf(i);
			BigInteger power= b1.pow(i);
			sum=sum.add(power);
		}
		String s=sum.toString();
		String str="";
		for(int i=s.length()-1;i>=s.length()-10;i--) {
			str=s.charAt(i)+str;
		}
		System.out.println(str);
	}
}
