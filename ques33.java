import java.util.Arrays;
import java.util.Scanner;

public class ques33{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of papers");
		int n=sc.nextInt();
		while(n>0) {
			System.out.println("Enter size of array");
			int s=sc.nextInt();
			int a[]=new int[s];
			for(int i=0;i<a.length;i++)
				a[i]=sc.nextInt();
			Arrays.sort(a);
			System.out.println(Arrays.toString(a));
			n--;
		}
	}
}