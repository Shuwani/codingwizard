public class ques57 {

	public static void main(String[] args) {
		int n=0b000000000000000000000000000001111;
		System.out.println(count(n));
	}
	
	public static int count(int x) {
		int c=0;
		while(x>0) {
			x&=(x-1);
			c++;
		}
		return c;
	}
}
