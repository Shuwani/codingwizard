import java.util.Scanner;

public class ques36 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
		int a[] = new int[n+2];
		catalan(a,n);
		for (int i=0;i<n;i++) 
			System.out.print(a[i] + " ");
	}
	
	public static int[] catalan(int a[], int n){
		a[0] = 1;
		a[1] = 1;
		for (int i=2;i<=n;i++) {
			a[i] = 0;
			for (int j=0;j<i;j++) {
				a[i]+= a[j]*a[i-j-1];
				}
			}
		return a;
	}
}