public class ques81 {
	public static void main(String[] args) {
		int arr[] = {1,5,3,2};
		System.out.print(balance(arr));
		}
	
	public static int balance(int []a){
		int n=a.length;
		int sum1=0;
		for(int i=0;i<n/2;i++) {
			sum1+=a[i];
		}
		int sum2=0;
		for (int i=n/2;i<n;i++) {
			sum2+= a[i];
		}
		int diff=Math.abs(sum1-sum2);
		return diff;
	}
}