import java.util.Scanner;

public class ques28 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number");
		int n=sc.nextInt();
		System.out.println("Sum of series= "+sumofseries(n));
	}
	public static int sumofseries(int n) {
		int sum=0;
		for(int i=1;i<=n;i++) {
			sum+=(int)Math.pow(i,(n-(i-1)));
		}
		return sum;
	}
}
