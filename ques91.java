public class ques91 {
	public static void main(String[] args) {
		long n=600851475143L;
		long max=0;
		for(long i=3;i<n/2;i+=2) {
			while(n%i==0) {
				n/=i;
			}
		}
		if(n>2) {
			max=n;
			System.out.println("Largest Prime Factor = "+max);
		}				
	}
}