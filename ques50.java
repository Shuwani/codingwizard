import java.util.Scanner;

public class ques50 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int count=0;
		System.out.println("Entert starting number and ending number.");
		int start=sc.nextInt();
		int end=sc.nextInt();
		for(int i=start+1;i<=end;i++) {
			boolean res=prime(i);
			if(res)
				count++;
		}
		System.out.println(count);
	}
	public static boolean prime(int n) {
		for(int i=2;i<=Math.sqrt(n);i++) {
			if(n%i==0)
				return false;
		}
		return true;
	}
}