import java.util.Scanner;

public class ques47 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		if(armstrong(n)==n)
			System.out.println("Armstrong Number");
		else
			System.out.println("Not an Armstrong Number");
	}
	public static int armstrong(int n) {
		int sum=0;
		int temp=n;
		while(temp>0) {
			int rem=temp%10;
			sum+=(int)Math.pow(rem,3);
      		temp/=10;
		}
		return sum;
	}
}