import java.math.BigInteger;
import java.util.Scanner;

public class ques02 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
        System.out.println(fibonacci(n));
	}
	public static BigInteger fibonacci(int n){
		BigInteger a=BigInteger.valueOf(0);
	    BigInteger b=BigInteger.valueOf(1);
	    BigInteger c;
	    for (int i=2;i<=n;i++){
	    	c=a.add(b);
	        a=b;
	        b=c;
	        }
	    return b;
	    }
	}