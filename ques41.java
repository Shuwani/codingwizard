import java.math.BigInteger;
import java.util.Scanner;

public class ques41{
	public static void main(String[]args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of array");
		int n=sc.nextInt();
		int m=sc.nextInt();
		int a[]=new int[n];
		System.out.println("Enter elements in the array");
		for(int i=0;i<n;i++) {
			a[i]=sc.nextInt();
		}
		subsequence(a.length,a,m);
	}
	public static void subsequence(int n,int arr[],int m){
		int sum = 0;
		for (int i = 0; i < n; i++)
			sum += arr[i];
		
		boolean[][] dp = new boolean[n + 1][sum + 1];
		for (int i = 0; i <= n; i++)
			dp[i][0] = true;
		
		for (int i = 1; i <= n; i++){
			dp[i][arr[i - 1]] = true;
			for (int j = 1; j <= sum; j++){
				if (dp[i - 1][j] == true){
					dp[i][j] = true;
					dp[i][j + arr[i - 1]] = true;
					}
				}
			}
		for (int j = 0; j <= sum; j++) {
			if (dp[n][j] == true) {
				if(j==0) {
					continue;
				}
				if(j%m==0) {
					System.out.println("Yes");
					break;
				}
				else {
					continue;
				}
			}
		}
	}
}