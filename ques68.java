public class ques68 {

	public static void main(String[] args) {
		int arr[] = { 12, 10, 5, 6, 52, 36 };
	    int pos=2;
	    split(arr,pos); 
	    for (int i=0; i<arr.length;i++) {
	    	System.out.print(arr[i] +" ");
	    }
	}
	public static int[] split(int arr[],int pos){
		int n=arr.length;
		for (int i=0;i<pos;i++) {
			int a =arr[0];
	        for (int j=0;j<n-1;j++) {
	        	arr[j]=arr[j+1];
	        }
	        arr[n-1]=a;
	    }
		return arr;
	}
}