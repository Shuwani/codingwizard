public class ques45 {
	public static int weight(int x) {
		int n=5;
		for(int i=0;i<n-1;i++) {
			if(((x>>i)&1)!=((x>>(i+1))&1)) {
				x ^= (1 << i) | (1 << (i + 1)); 
				break;
			}}
			return x;
		}
	public static void main(String[] args) {
		int x=0b10001;
		System.out.println(Long.toBinaryString(weight(x)));
	}
}
