import java.util.Scanner;
class Complex{
	double real,imag;

	void setData(double r,double i){
		real=r;
		imag=i;
	}
	
	void display(){
		System.out.print(real+" ");
		if(imag>=0)
			System.out.println("+"+imag+"i");
		else
			System.out.println(imag+"i");
	}
	
	public Complex add(Complex ob1,Complex ob2){
		Complex ob3=new Complex();
		ob3.real=ob1.real+ob2.real;
		ob3.imag=ob1.imag+ob2.imag;
		return(ob3);
	}
}

public class ques55{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		Complex c1=new Complex();
		Complex c2=new Complex();
		Complex c3=new Complex();
		System.out.println("Enter real and imaginary part of 1st number");
		double r1=sc.nextDouble();
		double i1=sc.nextDouble();
		c1.setData(r1, i1);
		System.out.println("Enter real and imaginary part of 2nd number");
		double r2=sc.nextDouble();
		double i2=sc.nextDouble();
		c2.setData(r2, i2);
		c1.display();
		c2.display();
		System.out.print("Sum = ");
		c3=c3.add(c1, c2);
		c3.display();
	}
}