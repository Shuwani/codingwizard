import java.util.Scanner;

public class ques65 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);  
		System.out.print("Enter the number you want to check: ");  
		int n=sc.nextInt();  
		int res=unique(n);
		if(res==1)
			System.out.println("The number is unique.");
		else
			System.out.println("The number is not unique.");
	}
	public static int unique(int number) {  
		int count = 0;  
		int num1 = number;
		int num2 = number;  
		while (num1>0){  
			int rem1= num1%10;  
			while (num2>0){
				int rem2=num2%10;  
				if(rem1==rem2){
					count++;
					}
				num2=num2/10;
				}
			num1=num1/10;
			}
		return count;
	}
}