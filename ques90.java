import java.util.Scanner;

public class ques90 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		System.out.println(reverse(n));
	}
	public static int reverse(int num) {
		int res=0;
		while(num!=0) {
			res=res*10+num%10;
			num=num/10;
		}
		return res;
	}
}