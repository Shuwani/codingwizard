import java.util.Scanner;

public class ques39 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of testcases");
		int count=sc.nextInt();
		while(count>0) {
			int start=sc.nextInt();
			int end=sc.nextInt();
			for(int i=start;i<=end;i++) {
				if(i==1)
					continue;
				boolean res=prime(i);
				if(res)
					System.out.println(i);
			}
			count--;
		}
	}
	public static boolean prime(int n) {
			for(int i=2;i<=Math.sqrt(n);i++) {
				if(n%i==0)
					return false;
			}
			return true;
	}
}