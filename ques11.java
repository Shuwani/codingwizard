import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ques11 {
	public static void main(String[] args) {
		path();
	}
	public static void path(){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the total no. of paths you want to compare for : ");
		List l1=new ArrayList();
		int n=sc.nextInt();
		while(n>0) {
			System.out.println("\nEnter X and Y coordinates of Initial position : ");
			int x1=sc.nextInt();
			int y1=sc.nextInt();
			System.out.println("Enter X and Y coordinates of Final position : ");
			int x2=sc.nextInt();
			int y2=sc.nextInt();
			int diff1=x2-x1;
			int diff2=y2-y1;
			double path1=Math.sqrt((int)Math.pow(diff1, 2)+(int)Math.pow(diff2, 2));
			l1.add((int)path1);
			System.out.printf("%.2f is the distance covered in path2 : ",path1);
			n--;
		}
		System.out.println();
		System.out.println(Collections.min(l1));
	}
}