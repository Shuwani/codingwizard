import java.util.Scanner;

public class ques69 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
		System.out.println(factors(n));
	}
	public static int factors(int n) {
		int sum=0;
		if(n%2!=0) {
			return 0;
		}
		else {
			for(int i=2;i<=n;i+=2) {
				if(n%i==0) {
					sum+=i;
				}
			}
			return sum;
		}
	}
}