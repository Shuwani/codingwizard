import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ques93 {
	public static void main(String[] args) {
		BigInteger num=BigInteger.valueOf(2);
		BigInteger power= num.pow(1000);
		String s=power.toString();
		int sum=0;
		for(int i=0;i<s.length();i++) {
			int dig =Character.getNumericValue(s.charAt(i));
			sum+=dig;
		}
		System.out.println(sum);
	}
}