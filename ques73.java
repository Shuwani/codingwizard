public class ques73 {

	public static void main(String[] args) {
		double n=30;
		System.out.println(find(n));
	}
	public static double find(double n){	
		double error=0.000001,den;
		n=n*(Math.PI/180);

		double x=n;
		double sinx=n;		
		double value=Math.sin(n);	
		int i=1;
		do{
			den=2*i*(2*i+1);
			x=-x*n*n/den;
			sinx=sinx+x;
			i = i + 1;
			} 
		while (error<=value-sinx);
		return sinx;
	}
}