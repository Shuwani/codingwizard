import java.util.Scanner;

public class ques07 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		double a=sc.nextFloat();
		System.out.println(area(a));
	}
	public static double area(double a){
		double pi = 3.14;
		double ans=(pi/4)*a*a;
		return ans;
	}
}