public class ques89 {

	public static void main(String[] args) {
		int arr[][] = {{ 2, 7, 6 },{ 9, 5, 1 },{ 4, 3, 8 }};
		magic(arr);
	}
	
	public static void magic(int arr[][]){
		int n=3;
		int sum1 =0,sum2=0;
		for (int i=0; i<n; i++){
			sum1+= arr[i][i];
			sum2+= arr[i][n-1-i];
			}
		if(sum1!=sum2)
			System.out.println("Not a magic Square");
		
		for (int i=0;i<n;i++) {
			int sr=0,sc=0;
			for(int j=0; j<n; j++){
				sr+=arr[i][j];
				sc+=arr[j][i];
			}
			if(sr!=sc || sc!=sum1)
				System.out.println("Not a magic Square");
			}
		System.out.println("Magic Square");
	}
}