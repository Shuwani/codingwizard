public class ques86 {	
	public static void main(String[] args) {
		for (int i=1; i<1000; i++) {
			if(kaprekar(i)==1)
				System.out.print(i+ " ");
		}
	}
	
	public static int kaprekar(int n){
		if (n == 1)
			return 1;
		
		int square=n*n;
		int count=0;
		while(square!= 0){
			count++;
			square/=10;
			}
		square=n*n;
		
		for(int i=1;i<count;i++){
			int num=(int) Math.pow(10,i);
			if (num==n)
				continue;

			int sum=(square/num)+(square%num);
			if(sum==n)
				return 1;
			}
		return 0;
		}
}