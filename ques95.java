import java.math.BigInteger;
import java.util.Scanner;
public class ques95 {
	public static void main(String[] args) {
		long num=2000000L;
		BigInteger sum=BigInteger.valueOf(0);
		for(long i=2;i<=num;i++) {
			boolean res=prime(i);
			if(res)
				sum=sum.add(BigInteger.valueOf(i));
		}
		System.out.println(sum);
	}
	public static boolean prime(long n) {
			for(long i=2;i<=Math.sqrt(n);i++) {
				if(n%i==0)
					return false;
			}
			return true;
	}
}