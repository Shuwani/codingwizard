public class ques01 {
	
	public static void main(String[] args) {
		int n=10;
		System.out.println(count(n));
	}
	
	public static int count(int n){
			if (n==1) 
				return 0;
			if (n==2) 
				return 1;
			int res=(n-1)*(count(n-1)+count(n-2));
			return res;
	}
}
