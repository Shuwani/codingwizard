import java.util.Scanner;

public class ques53 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n");
		int n=sc.nextInt();
		print(n);
	}
	public static void print(int n) {
		for(int i=1;i<=n/2;i++) {
			System.out.print(i*i+" ");
			System.out.print(i*i*i+" ");
		}
	}
}
