import java.util.Scanner;

public class ques56 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int n=sc.nextInt();
		if(magic(n)==1)
			System.out.println("Magic Number");
		else
			System.out.println("Not a magic Number");
	}
	
	public static int magic(int n) {
		int sum=0;
		int sum1=0;
		while(n>0) {
			sum=sum+(n%10);
			n/=10;
		}
		while(sum>0) {
			sum1=sum1+(sum%10);
			sum/=10;
		}
		return sum1;
	}
}