
public class ques97 {

	public static void main(String[] args) {
		int num= 200;
		int[] coin = {1,2,5,10,20,50,100,200};
		int[] ways = new int[num+1];
		System.out.println(get(coin,num,ways));
	}
	public static int get(int coin[],int n,int a[]) {
		a[0]=1;
		for (int i=0; i<coin.length;i++) {
			for (int j=coin[i];j<=n; j++) {
				a[j] += a[j-coin[i]];
			}
		}
		return a[n];
	}
}