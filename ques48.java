public class ques48{
	public static long power(double x,int y) {
		long res=1;
		if(y<0) {
			x=1/x;
			y=-y;
		}
		while(y!=0) {
			if((y&1)!=0) {
				res*=x;
			}
			x*=x;
			y>>=1;
		}
		return res;
	}

	public static void main(String[] args) {
		double x=2.0;
		int y=3;
		System.out.println((double)power(x,y));
	}
}