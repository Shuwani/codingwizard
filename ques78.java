import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ques78 {

	public static void main(String[] args) {
		Integer a[]= {1,2,3,4,5,6,7,8};
		int n=3;
		swappp(a,n);
	}
	public static void swappp(Integer a[],int n) {
		List<Integer> li=Arrays.asList(a);
		Collections.swap(li,n-1,a.length-n);
		for(int i=0;i<li.size();i++)
			System.out.print(li.get(i)+" ");
	}
}
