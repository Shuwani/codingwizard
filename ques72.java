import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ques72{
	public static List<Integer> applyPermutation(Integer[] a, Integer[] p){
		List<Integer> A=Arrays.asList(a);
		List<Integer> P=Arrays.asList(p);
		int next,temp;
		int size=A.size();
		for(int i=0;i<size;i++) {
			next=i;
			while(P.get(next)>=0) {
				Collections.swap(A,i,P.get(next));
				temp=P.get(next);
				P.set(next,P.get(next)-size);
				next=temp;
			}
		}
		return A;
		}
	public static void main(String[] args) {
		Integer a[]= {50,40,70,60,90};
		Integer p[]= {3,0,4,1,2};
		List<Integer> li=applyPermutation(a,p);
		Integer[] b= new Integer[li.size()];
		for (int i=0;i<li.size();i++)
            b[i]=li.get(i);
		System.out.println("  arr[]= "+Arrays.toString(b));
		Integer[] c= new Integer[b.length];
		for(int i=0;i<b.length;i++)
			c[i]=i;
		System.out.println("index[]= "+Arrays.toString(c));
	}
}