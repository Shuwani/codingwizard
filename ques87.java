import java.util.Scanner;

public class ques87 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Input a float number : ");
		double n=sc.nextDouble();
		round(n);
	}
	public static void round(double num) {
		System.out.printf("The rounded value of %.6f is : ",num);
		System.out.println((double)Math.round(num));
	}
}
