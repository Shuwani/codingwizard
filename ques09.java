import java.math.BigInteger;
import java.util.Scanner;

public class ques09{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=100;
		BigInteger num=factorial(n);
		System.out.println(num);
		sumDigits(num.toString());
	}

	public static BigInteger factorial(int n) {
			BigInteger fact = BigInteger.ONE;
			for (int i=1;i<=n;i++) {
				fact=fact.multiply(BigInteger.valueOf(i));
			}
			return fact;
		}

	public static void sumDigits(String s) {
		long sum = 0;
		for (int i=0; i<s.length(); i++) {
			String ans=s.substring(i, i + 1);
			sum += Long.parseLong(ans);
		}
		System.out.println(sum);
	}
}