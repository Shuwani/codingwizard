
public class ques94 {

	public static void main(String[] args) {
		long n =10;
		System.out.println(lcm(n));
	}
	public static long gcd(long a, long b){
		if(a%b!= 0)
			return gcd(b,a%b);
		else
			return b;
	}
	
	public static long lcm(long n){
		long ans = 1;
		for (long i = 1; i <= n; i++)
			ans = (ans * i)/(gcd(ans, i));
		return ans;
	}
}