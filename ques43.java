import java.util.Scanner;
public class ques43 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		convert(num);
	}
	public static void convert(int num){
		String hexa = Integer.toHexString(num);
		System.out.println("HexaDecimal Value is : " + hexa);
		String octal = Integer.toOctalString(num);
		System.out.println("Octal Value is : " + octal);
		String binary = Integer.toBinaryString(num);
		System.out.println("Binary Value is : " + binary);
	}
}