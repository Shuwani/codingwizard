public class ques22 {
  public static void main(String[] args){
    long x=0b1101;
    long y=0b1001;
    System.out.println(Long.toBinaryString(multiply(x,y)));
    }
  public static long multiply(long x,long y){
    long sum =0,k=0;
    while (x !=0) {
      if ((x & 1) != 0) {
    	  sum = add(sum, y<<k);
      }
      k=k+1;
      x>>=1;
      }
    return sum;
   }  
    public static long add(long sum,long y) { 
      long carry=0;
      while(y!=0){
        carry=sum&y;
        sum=sum^y;
        y=carry<<1;
      }
      return sum;
    }}