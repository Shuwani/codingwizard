import java.util.Scanner;

public class ques15 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rod length");
		int n=sc.nextInt();
		int a[]={1, 5, 8, 9, 10, 17, 17, 20};
		System.out.println("Maximum value is : "+rodcut(a,n));
	}
	public static int rodcut(int price[], int n){
		if (n<=0)
			return 0;
		
		int max=0;
		for (int i=1;i<=n;i++) {
			max=Math.max(max,price[i-1] + rodcut(price, n-i));
			}
		return max;
		}
}