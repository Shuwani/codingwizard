public class ques23 {
	public static void main(String[] args) {
		int[] arr1 = {1, 4, 6, 8};
		int[] arr2 = {11, 19, 10, 7};
		mergeArrays(arr1,arr2);
	}
	public static void mergeArrays(int[] arr1, int[] arr2){
		int n1=arr1.length;
		int n2=arr2.length;
		int[] arr3 = new int[n1+n2];
		int i=0,j=0,k=0;
			for(i=0;i<n1;i++) {
				for(j=i;j<=i;j++) {
					arr3[k++]=arr1[i];
					arr3[k++]=arr2[j];
				}
			}
			System.out.println("Array after merging");
	        for (int x=0; x<n1+n2; x++)
	            System.out.print(arr3[x] + " ");
	    }
	}