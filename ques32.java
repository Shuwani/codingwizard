import java.util.Scanner;

public class ques32 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter n and m");
		int n=sc.nextInt();
		int m=sc.nextInt();
		System.out.println(fac(n)/(fac(m)*fac(n-m)));
	}
	public static int fac(int n) {
		int fact=1;
		for(int i=1;i<=n;i++) {
			fact*=i;
		}
		return fact;
	}
}
