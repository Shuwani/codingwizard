import java.util.*;
public class ques31 {
  public static void main(String[] args) {
     ArrayList<ArrayList<Integer>> li=new ArrayList<ArrayList<Integer>>();
     li.add(new ArrayList<Integer>(Arrays.asList(4,5,6,3)));
	 li.add(new ArrayList<Integer>(Arrays.asList(2,3,2,5)));
     li.add(new ArrayList<Integer>(Arrays.asList(6,1,2,7)));
     li.add(new ArrayList<Integer>(Arrays.asList(4,5,3,2)));
    matrixspiral(li);
    }
  
  public static void matrixspiral(ArrayList<ArrayList<Integer>> li){
	  List<Integer> spiral = new ArrayList <>();
	  for (int offset = 0; offset <1 ; offset++) {
		  matrixClockwise(li, offset, spiral);
		  }
	  int sum=0;
	  for(int i=0;i<spiral.size();i++) {
		  sum+=spiral.get(i);
	  }
	  System.out.println("Sum of bordered elements : "+ sum);
	 }
  
  private static void matrixClockwise(ArrayList<ArrayList<Integer>> sq ,int offset,List<Integer> spiral) {
	  for (int j=offset; j<sq.size()-offset-1; j++) {
		  spiral.add(sq.get(offset).get(j));
	  }
	  
	  // add 3,6 in matrix
	  for (int i =offset ;i<sq.size()-offset-1; i++){
		  spiral.add(sq.get(i).get(sq.size()-offset- 1));
	  }
	  
	  
	  // add 9,8 in matrix
	  for (int k = sq.size()-offset-1; k>offset; k--){
		  spiral.add(sq.get(sq.size()-offset-1).get(k));
		  }
	  
	  // add 7,4 in matrix
	  for (int l = sq.size()-offset-1; l>offset; l--) {
		  spiral.add(sq.get(l).get(offset));
		  }
	  }
  }
