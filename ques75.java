import java.util.Scanner;

public class ques75 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		long n=sc.nextInt();
		System.out.println(fact(n));
	}
	public static long fact(long n){
		if (n>1)
			return n*fact(n-2);
		return 1;
	}
}