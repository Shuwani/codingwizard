import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ques80 {

	public static void main(String[] args) {
		int a=8,b=4;
		if(anagram(a,b))
			System.out.println("Anagram numbers");
		else
			System.out.println("Not an Anagram numbers");
	}
	
	public static boolean anagram(int a,int b){ 
		String s=Integer.toBinaryString(a);
		String s1=Integer.toBinaryString(b);
		List l1= new ArrayList <>(Collections.nCopies(8, 0));
		List l2= new ArrayList <>(Collections.nCopies(8, 0));
		for(int i=0;i<s.length();i++) {
			int num1=Integer.parseInt(s.charAt(i)+"");
			l1.set(i,num1);
		}
		
		for(int i=0;i<s1.length();i++) {
			int num2=Integer.parseInt(s1.charAt(i)+"");
			l2.set(i,num2);
		}
		
		Collections.sort(l1);
		Collections.sort(l2);
		
		for (int i=0;i<8;i++) {
			if(l1.get(i)!=l2.get(i))
				return false;
		}
		return true;
	}
}