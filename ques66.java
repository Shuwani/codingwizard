import java.util.Scanner;

public class ques66 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);  
		System.out.print("Enter the number");  
		int n=sc.nextInt();  
		System.out.println(term(n));
	}
	public static int term(int n) {
		int ans=0;
		if (n%2!=0) {
			for (int i=0;i<=n/2;i++)
				ans=7*i;
			}
		else{
			for(int i=0;i<n/2;i++)
				ans=6*i;
		}
		return ans;
	}
}